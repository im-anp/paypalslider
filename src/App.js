
import './App.scss';
import React from 'react';
import data from './data.json';



const slideWidth = 30;

let _items = data;

if (window) {
  console.log("aaaaaa", decodeURIComponent(window.location.pathname).replace('/', ''))
  if (window.location.pathname != '/') {
    _items = data.filter((val) => {
      return val.category == decodeURIComponent(window.location.pathname).replace('/', '');
    })
  }
  _items.push(..._items)

}


const length = 5;



const sleep = (ms = 0) => {
  return new Promise(resolve => setTimeout(resolve, ms))
}

const createItem = (position, idx, activeIdx) => {
  const item = {
    styles: {
      transform: `translateX(${position * slideWidth}rem)`
    },
    player: _items[idx]
  }

  switch (position) {
    case length - 1:
    case length + 1:
      item.styles = { ...item.styles, filter: 'grayscale(1)' }
      break
    case length:
      break
    default:
      item.styles = { ...item.styles, opacity: 0 }
      break
  }

  return item
}

const CarouselSlideItem = ({ pos, idx, activeIdx }) => {
  const item = createItem(pos, idx, activeIdx)
  return (
    <li className='carousel__slide-item' style={item.styles}>
      <div className='carousel__slide-item-img-link'>
        <img src={item.player.image} alt={item.player.title} />
      </div>
      <div className='carousel-slide-item__body'>
        <h4>{item.player.title}</h4>
        <p>{item.player.description}</p>
      </div>
    </li>
  )
}

const keys = Array.from(Array(_items.length).keys())

const Carousel = () => {
  console.log('+++++', keys)
  const [items, setItems] = React.useState(keys)
  const [cat, setCat] = React.useState('');
  const [isTicking, setIsTicking] = React.useState(false)
  const [activeIdx, setActiveIdx] = React.useState(0)
  const bigLength = items.length

  const prevClick = (jump = 1) => {
    if (!isTicking) {
      setIsTicking(true)
      setItems(prev => {
        return prev.map((_, i) => prev[(i + jump) % bigLength])
      })
    }
  }

  const nextClick = (jump = 1) => {
    if (!isTicking) {
      setIsTicking(true)
      setItems(prev => {
        return prev.map(
          (_, i) => prev[(i - jump + bigLength) % bigLength]
        )
      })
    }
  }

  const handleChange = (e) => {
    if (e.target.value)
      window.location.href = "/" + e.target.value;
    else
      window.location.href = "/"
    setCat(e.target.value)
  }

  const handleDotClick = idx => {
    if (idx < activeIdx) prevClick(activeIdx - idx)
    if (idx > activeIdx) nextClick(idx - activeIdx)
  }

  React.useEffect(() => {
    if (isTicking) sleep(300).then(() => setIsTicking(false))

  }, [isTicking])

  React.useEffect(() => {
    setActiveIdx((length - (items[0] % length)) % length) // prettier-ignore
  }, [items])
  React.useEffect(() => {
    setCat(decodeURIComponent(window.location.pathname).replace('/', ''))
  }, [])

  return (
    <>
      <div className="filter">
        <label>Category Filter</label>
        <select onChange={handleChange} value={cat}>
          <option value="">Select All</option>
          <option value="men clothing">men clothing</option>
          <option value="jewelery">jewelery</option>
          <option value="electronics">electronics</option>
          <option value="women clothing">women clothing</option>
        </select>
      </div>

      <div className='carousel__wrap'>
        <div className='carousel__inner'>
          <button
            className='carousel__btn carousel__btn--prev'
            onClick={() => prevClick()}>
            <i className='carousel__btn-arrow carousel__btn-arrow--left' />
          </button>
          <div className='carousel__container'>
            <ul className='carousel__slide-list'>
              {items.map((pos, i) => (
                <CarouselSlideItem
                  key={i}
                  idx={i}
                  pos={pos}
                  activeIdx={activeIdx}
                />
              ))}
            </ul>
          </div>
          <button
            className='carousel__btn carousel__btn--next'
            onClick={() => nextClick()}>
            <i className='carousel__btn-arrow carousel__btn-arrow--right' />
          </button>
          <div className='carousel__dots'>
            {items.slice(0, length).map((pos, i) => (
              <button
                key={i}
                onClick={() => handleDotClick(i)}
                className={i === activeIdx ? 'dot active' : 'dot'}
              />
            ))}
          </div>
        </div>
      </div>
    </>
  )
}


export default Carousel;
